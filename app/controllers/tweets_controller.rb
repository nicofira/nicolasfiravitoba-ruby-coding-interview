class TweetsController < ApplicationController
  DEFAULT_NUMBER_OF_TWEETS = 10
  INITIAL_PAGE = 1

  def index
    render json: Tweet.all.limit(pagination_number_of_tweets).offset(offset_value).order(created_at: :desc)
  end

  private

  def pagination_page
    pagination_params[:page] || INITIAL_PAGE
  end

  def pagination_number_of_tweets
    pagination_params[:number_of_tweets] || DEFAULT_NUMBER_OF_TWEETS
  end

  def offset_value
    pagination_page * pagination_number_of_tweets
  end

  def pagination_params
    params.permit(:page, :number_of_tweets)
  end
end
