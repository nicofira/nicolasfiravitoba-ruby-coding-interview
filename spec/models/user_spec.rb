require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user1) { FactoryBot.create(:user, username: 'Mark') }
  let!(:user2) { FactoryBot.create(:user, username: 'Max') }
  let!(:user3) { FactoryBot.create(:user, username: 'John') }

  it 'return user by username using partial text' do
    expect(User.by_username('Ma').count).to be(2)
    expect(User.by_username('Ma')).to include(user1, user2)
  end
end
